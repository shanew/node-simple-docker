'use strict';

// Load modules

const Hapi = require('hapi');
const Assert = require('assert');

// Declare internals

const port = 3000;
const plugins = [
    require('hapi-auth-basic'),
    require('./plugin')
];

const server = new Hapi.Server();
server.connection({ port });


server.register(plugins, (err) => {

    Assert.ifError(err);

    server.start((err) => {

        Assert.ifError(err);

        console.log(`Server running on port: ${port}.`)
    });
});

