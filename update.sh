#!/bin/bash

# Exit on error
# set -e

docker-compose -f ./compose.yml build web

# now finally start the services
docker-compose -f ./compose.yml up --no-deps -d web