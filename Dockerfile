FROM mhart/alpine-node:6.4

# Default command to run
CMD [ "npm", "start" ]

# Set working directory.
ENV HOME=/home/app/example
WORKDIR $HOME

# Expose default port
EXPOSE 3000

# Create `app` system group.
# Then, create the `app` system user and add to the `app` group.
RUN addgroup -S app \
  && adduser -S -G app app

# Install app dependencies
# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
ADD package.json npm-shrinkwrap.json /tmp/

RUN cd /tmp \
  && npm cache clean \
  && npm install \
  && yes | cp -rf /tmp/node_modules $HOME \
  && cd $HOME

# Bundle app source
ADD . ./

# Grant permissions of the app group and the app user to this directory.
RUN chown -R app:app ./

# Change the default user from root to app.
USER app
