'use strict';

// Load modules

const Joi = require('joi');
const Boom = require('boom');
const Bcrypt = require('bcryptjs');

// Declare internals

const internals = {};
const users = {};
internals.saltRounds = 10;

internals.register = function (request, reply) {

    if (users.hasOwnProperty(request.payload.email)) {
        return reply(Boom.badRequest('User with email already exists.'));
    }

    internals.hashPassword(request.payload.password, (err, hash) => {

        if (err) {
            return reply(Boom.badImplementation('Failed to hash password.'));
        }

        console.log(hash);
        users[request.payload.email] = hash;

        return reply().code(200);
    });
};

internals.hashPassword = function (password, next) {

    Bcrypt.genSalt(internals.saltRounds, (err, salt) => {

        if (err) {
            return next(err);
        }

        return Bcrypt.hash(password, salt, next);
    });
};

internals.validate = function (request, email, password, next) {

    return Bcrypt.compare(password, users[email], (err, valid) => {

        next(err, valid, { email });
    });
};


internals.me = function(request, reply) {

    reply({ message: `Hello ${request.auth.credentials.email}!` });
};

// Plugin

const register = function (server, options, next) {

    server.auth.strategy('simple', 'basic', { validateFunc: internals.validate });

    server.route({
        method: 'GET',
        path: '/me',
        config: {
            auth: 'simple',
            handler: internals.me
        }
    });

    server.route({
        method: 'POST',
        path: '/register',
        config: {
            handler: internals.register,
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().min(8).required()
                }
            }
        }
    });

    return next();
};




register.attributes = {
    name: 'api'
};

exports = module.exports = register;
